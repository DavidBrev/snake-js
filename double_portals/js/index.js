const gridWidth = 20;
const gridHeight = 20;
const baseTimeInterval = 125;
const timeSubstract = 2.5;
const timeLimit = 37.5;
const startingLength = 3;
const startingDir = 1;
const walls = false;

$("#commandPause").html("<strong>Enter</strong> - Start, Pause, Unpause");
$("#commandNavigationLeft").html("<strong>WASD/ZQSD</strong> - change snake heading");
$("#commandNavigationRight").html("<strong>Arrow Keys</strong> - change snake heading");

const gridLeft = $("#gridLeft");
const pointsLeft = $("#pointsLeft");
const displayLeft = $("#gameDataLeft");

const gridRight = $("#gridRight");
const pointsRight = $("#pointsRight");
const displayRight = $("#gameDataRight");

let GameLeft = GenerateGame(gridLeft, pointsLeft, displayLeft, gridWidth, gridHeight, baseTimeInterval, timeSubstract, timeLimit, startingLength, startingDir, walls);
let GameRight = GenerateGame(gridRight, pointsRight, displayRight, gridWidth, gridHeight, baseTimeInterval, timeSubstract, timeLimit, startingLength, startingDir, walls);
let started = false;
let pause = false;

function keydownHandler(e) {
  switch(e.code){
    case "Enter":
      if(!started){
        GameLeft.Start();
        GameRight.Start();
        started = true;
      }
      else if(!pause){
        pause = true;
        GameLeft.Pause();
        GameRight.Pause();
      }
      else{
        pause = false;
        GameLeft.Unpause();
        GameRight.Unpause();
      }
      break;
    case "ArrowUp":
      GameRight.ChangeDirection(0);
      break;
    case "ArrowRight":
      GameRight.ChangeDirection(1);
      break;
    case "ArrowDown":
      GameRight.ChangeDirection(2);
      break;
    case "ArrowLeft":
      GameRight.ChangeDirection(3);
      break;
    case "KeyW":
      GameLeft.ChangeDirection(0);
      break;
    case "KeyD":
      GameLeft.ChangeDirection(1);
      break;
    case "KeyS":
      GameLeft.ChangeDirection(2);
      break;
    case "KeyA":
      GameLeft.ChangeDirection(3);
      break;
    default:
  }
}

$("#newGame").on("click", e => {
  started = false;
  pause = false;
  GameLeft.Destroy();
  GameRight.Destroy();
  GameLeft = GenerateGame(gridLeft, pointsLeft, displayLeft, gridWidth, gridHeight, baseTimeInterval, timeSubstract, timeLimit, startingLength, startingDir, walls);
  GameRight = GenerateGame(gridRight, pointsRight, displayRight, gridWidth, gridHeight, baseTimeInterval, timeSubstract, timeLimit, startingLength, startingDir, walls);
})

document.addEventListener('keydown', keydownHandler);